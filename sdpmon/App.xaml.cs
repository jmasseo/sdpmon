﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Hardcodet.Wpf.TaskbarNotification;
using System.Timers;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Navigation;



namespace sdpmon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public TaskbarIcon notifyIcon;
        public Parser p;
        public string url;
        public string tech_key;
        public string views_to_watch;
        public int refreshtime;
        List<string> viewlist;
        public System.Collections.Specialized.StringCollection vlist;
        public DispatcherTimer dt;
        public List<int> knowntickets;
        public bool autoenabled;
        public smPopup myPopup;
        Dictionary<string, string> view_to_viewList;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            //create the notifyicon (it's a resource declared in NotifyIconResources.xaml
            notifyIcon = (TaskbarIcon)FindResource("NotifyIcon");
            
            knowntickets = new List<int>();
            GetOptions();
            p = new Parser();
            p.url = url;
            p.tkey = tech_key;
            p.myApp = this;
            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, refreshtime);
            dt.Tick += new EventHandler(dispatcherTime_Tick);
            if (autoenabled) dt.Start();
            
            
        }
        private void dispatcherTime_Tick(object sender, EventArgs e)
        {
            p.SendAllRequestsRequest();
        }
        protected override void OnExit(ExitEventArgs e)
        {
            notifyIcon.Dispose(); //the icon would clean up automatically, but this is cleaner
            base.OnExit(e);
        }
        public void GetOptions()
        {
            url = sdpmon.Properties.Settings.Default.URL;
            tech_key = sdpmon.Properties.Settings.Default.TechnicianKey;
            vlist = sdpmon.Properties.Settings.Default.ViewsToWatch;
            refreshtime = (int)sdpmon.Properties.Settings.Default.RefreshInterval;
            autoenabled = sdpmon.Properties.Settings.Default.autoenabled;
        }
        public void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Console.WriteLine("RequestNavigate:" + e.Uri.ToString());
            string urlstring = e.Uri.ToString();
            if (sdpmon.Properties.Settings.Default.BrowserPath != "")
            {
                System.Diagnostics.Process.Start(sdpmon.Properties.Settings.Default.BrowserPath,urlstring);
            }
            else
            {
                
                System.Diagnostics.Process.Start(urlstring);
            }
        }
    }
}
