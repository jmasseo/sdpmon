﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Navigation;
using System.IO;
using System.Net;
using System.Web;
using System.Drawing;
using Hardcodet.Wpf.TaskbarNotification;
using System.Windows.Media;
using System.Media;

namespace sdpmon
{
    public class RequestState
    {
        public HttpWebRequest request;
        public string filter;
    }
    public class Parser
    {
        public HttpWebRequest request;
        public string url;
        public string tkey;
        public List<Ticket> tickets;
        public List<Filter> filters;
        public Dictionary<string, List<Ticket>> myDict;
        public Dictionary<string, List<Ticket>> newTickets;
        public App myApp;
        private int outrqs;
        public Parser()
        {
            tickets = new List<Ticket>();
            outrqs = 0;
            if (sdpmon.Properties.Settings.Default.FilterList == null) sdpmon.Properties.Settings.Default.FilterList = new System.Collections.Specialized.StringCollection();
            filters = Filter.LOAD_SC(sdpmon.Properties.Settings.Default.FilterList);
            myDict = new Dictionary<string, List<Ticket>>();
            newTickets = new Dictionary<string, List<Ticket>>();
        }
        public bool HaveTicket(string f, int woid)
        {
            if(myDict.ContainsKey(f))
            {
                foreach(Ticket t in myDict[f])
                {
                    if(t.workorderid == woid)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        
        public void SendFiltersRequest()
        {
            request = WebRequest.CreateHttp(url);
            request.Method = "POST";
            StringBuilder postData = new StringBuilder();

            postData.Append(String.Format("{0}={1}&", "OPERATION_NAME", HttpUtility.UrlEncode("GET_REQUEST_FILTERS")));

            postData.Append(String.Format("{0}={1}&", "TECHNICIAN_KEY", HttpUtility.UrlEncode(tkey)));
            postData.Append(String.Format("format={0}", HttpUtility.UrlEncode("xml")));
            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postBytes = ascii.GetBytes(postData.ToString());

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postBytes.Length;

            //request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";

            Stream postStream = request.GetRequestStream();
            postStream.Write(postBytes, 0, postBytes.Length);
            postStream.Flush();
            postStream.Close();
            outrqs++;
            request.BeginGetResponse(GetFiltersResponseCallback, null);

        }
        public string GetFilterNameByValue(string f)
        { 
            foreach(Filter fi in filters)
            {
                if(fi.value == f)
                {
                    return fi.name;
                }
            }
            return f;
        }

        public string GetFilterValueByName(string f)
        {
            foreach (Filter fi in filters)
            {
                if (fi.name == f)
                {
                    return fi.value;
                }
            }
            return f;
        }

        void GetFiltersResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                outrqs--;
                WebResponse resp = request.EndGetResponse(asynchronousResult);
                HttpWebResponse response = (HttpWebResponse)resp;
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                // Close the stream object
                //streamResponse.Close();
                streamRead.Close();
                // Release the HttpWebResponse
                response.Close();

                //Do whatever you want with the returned "responseString"
                Console.WriteLine(responseString);
                List<Filter> t = parsefilters(responseString);
                filters = t;
                
                //ticketgrid.Items.Add(t); 
            }
            catch { return; }
        }
        public void SendAllRequestsRequest()
        {
            foreach(Filter f in filters)
            {
                if(f.enabled)
                {
                    SendRequestsRequest(f.value);
                }
            }
        }
        public void SendRequestsRequest(Filter f)
        {
            SendRequestsRequest(f.value);
        }
        public void SendRequestsRequest(string f)
        {
            outrqs++;
            RequestState rs = new RequestState();
            rs.filter = f;
            rs.request = WebRequest.CreateHttp(url);
            rs.request.Method = "POST";
            //System.IO.StreamReader myFile = new System.IO.StreamReader("get_requests.xml");

            string myString = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
<Details>
  <parameter>
    <name>from</name>
    <value>0</value>
  </parameter>
  <parameter>
    <name>limit</name>
    <value>25</value>
  </parameter>
  <parameter>
    <name>filterby</name>
    <value>All_Pending_User</value>
  </parameter>
</Details>";
            myString = myString.Replace("All_Pending_User", f);
            //myFile.Close();
            Console.Write(myString);
            StringBuilder postData = new StringBuilder();

            postData.Append(String.Format("{0}={1}&", "OPERATION_NAME", HttpUtility.UrlEncode("GET_REQUESTS")));

            postData.Append(String.Format("{0}={1}&", "TECHNICIAN_KEY", HttpUtility.UrlEncode(tkey)));
            postData.Append(String.Format("format={0}&", HttpUtility.UrlEncode("xml")));
            postData.Append(String.Format("INPUT_DATA={0}&", HttpUtility.UrlEncode(myString)));
            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] postBytes = ascii.GetBytes(postData.ToString());

            rs.request.Method = "POST";
            rs.request.ContentType = "application/x-www-form-urlencoded";
            rs.request.ContentLength = postBytes.Length;

            //request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            try
            {
                Stream postStream = rs.request.GetRequestStream();
           
            postStream.Write(postBytes, 0, postBytes.Length);
            postStream.Flush();
            postStream.Close();
            
            rs.request.BeginGetResponse(GetRequestsResponseCallback, rs);
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        void GetRequestsResponseCallback(IAsyncResult asynchronousResult)
        {
            outrqs--;
            RequestState rs = (RequestState)asynchronousResult.AsyncState;
            string responseString;
            try
            {
                
                WebResponse resp = rs.request.EndGetResponse(asynchronousResult);
                
                HttpWebResponse response = (HttpWebResponse)resp;
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                responseString = streamRead.ReadToEnd();
                // Close the stream object
                //streamResponse.Close();
                streamRead.Close();
                // Release the HttpWebResponse
                response.Close();

                //Do whatever you want with the returned "responseString"
                Console.WriteLine(responseString);
            }
            catch { return; }
            
            List<Ticket> t = parserequests(responseString);
            tickets = t;
             
            
                foreach(Ticket nt in tickets)
                {
                    if(!HaveTicket(rs.filter,nt.workorderid))
                    {
                        if(!newTickets.ContainsKey(rs.filter))
                        {
                            newTickets[rs.filter] = new List<Ticket>();
                           
                            
                        }
                        newTickets[rs.filter].Add(nt);
                    }
                }
                myDict[rs.filter] = tickets;
                if(outrqs == 0 && newTickets.Count > 0)
                {
                    
                    StringBuilder ns = new StringBuilder();
                    if (myApp.myPopup == null||!myApp.myPopup.IsVisible) myApp.myPopup = new smPopup();
                    myApp.myPopup.p = this;
                    myApp.myPopup.ucTextBox.Inlines.Clear();
                    foreach (string fl in newTickets.Keys)
                    {
                        myApp.myPopup.ucTextBox.Inlines.Add(new Span(new Bold(new Run("Group: " + GetFilterNameByValue(fl)))));
                        myApp.myPopup.ucTextBox.Inlines.Add(new LineBreak());
                        foreach (Ticket nt in newTickets[fl])
                        {
                            Inline x = new Span(new Italic(new Run(nt.workorderid.ToString()))) ;
                            Run y = new Run(nt.subject);
                            Hyperlink h = new Hyperlink(y);
                            h.RequestNavigate += myApp.Hyperlink_RequestNavigate;
                            h.NavigateUri = new Uri(/*nt.uri*/ sdpmon.Properties.Settings.Default.ViewWOURL + nt.workorderid.ToString());
                            myApp.myPopup.ucTextBox.Inlines.Add(x);
                            myApp.myPopup.ucTextBox.Inlines.Add(h);
                            myApp.myPopup.ucTextBox.Inlines.Add(new LineBreak());
                            ns.AppendLine("SR" + nt.workorderid.ToString() + " " + nt.subject);
                        }
                    }
                    //myApp.notifyIcon.ShowBalloonTip(rs.filter, ns.ToString(), BalloonIcon.Info);
                    myApp.myPopup.BalloonText = "New Ticket Alert!";
                    SystemSounds.Exclamation.Play();
                    if(!myApp.myPopup.IsVisible) myApp.notifyIcon.ShowCustomBalloon(myApp.myPopup, PopupAnimation.Slide, null);
                    //newTickets = new Dictionary<string, List<Ticket>>();
                }
                //ticketgrid.Items.Add(t); 
           
        }
       
        public List<Filter> parsefilters(string text)
        {
            List<Filter> results = new List<Filter>();
            XmlSerializer xSerializer = new XmlSerializer(typeof(operation));
            
            
            StringReader xy = new StringReader(text);
            

            operation myResponse = (operation)xSerializer.Deserialize(xy);


            foreach(operationDetailsFiltersParameter x in myResponse.Details.First())
            {
                Filter nf = new Filter();
                nf._name = x.value;
                nf._value = x.name;
                results.Add(nf);
            }
                

            return results;
        }
        public List<Ticket> parserequests(string text)
        {
            List<Ticket> results = new List<Ticket>();
            XmlSerializer xSerializer = new XmlSerializer(typeof(API));
            
            
            StringReader xy = new StringReader(text);
            

            API myResponse = (API)xSerializer.Deserialize(xy);


            foreach(APIResponseOperation x in myResponse.response)
            {

                APIResponseOperationResult z = x.result.First();
                APIResponseOperationDetails arod = x.Details.First();
                if (arod.record == null) return results;
                foreach(APIResponseOperationDetailsRecord rec in arod.record)
                {
                    Ticket nt = new Ticket();
                    nt.uri = rec.URI;
                    foreach(APIResponseOperationDetailsRecordParameter p in rec.parameter)
                    {
                        switch(p.name)
                        {
                            case "status":
                                nt.status = p.value;
                                break;
                            case "isoverdue":
                                nt.isoverdue = bool.Parse(p.value);
                                break;
                            case "workorderid":
                                nt.workorderid = int.Parse(p.value);
                                break;
                            case "createdtime":
                                nt.createdtime = long.Parse(p.value);
                                break;
                            case "requester":
                                nt.requester = p.value;
                                break;
                            case "duebytime":
                                nt.duebytime = long.Parse(p.value);
                                break;
                            case "subject":
                                nt.subject = p.value;
                                break;
                            case "technician":
                                nt.technician = p.value;
                                break;
                            case "priority":
                                nt.priority = p.value;
                                break;
                            case "createdby":
                                nt.createdby = p.value;
                                break;
                        }
                    }
                    results.Add(nt);

                }
            }




            return results;
                        

        }
    }
}
