﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;

namespace sdpmon
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<string> stc;
        public bool testenabled { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            stc = new ObservableCollection<string>();
            stc.Clear();
            if(sdpmon.Properties.Settings.Default.ViewsToWatch == null)
            {
                sdpmon.Properties.Settings.Default.ViewsToWatch = new StringCollection();
                sdpmon.Properties.Settings.Default.Save();
            }
            foreach (string s in sdpmon.Properties.Settings.Default.ViewsToWatch)
            {
                stc.Add(s);
            }
            VIEWSTOWATCH.ItemsSource = stc;
        }

        private void RefreshInterval_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }
        private static bool IsTextAllowed(string text)
        {
            try
            {
                int nint = Int16.Parse(text);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void OK_Button_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*
            string text;
            System.IO.StreamReader myFile = new System.IO.StreamReader("D:\\scratch\\example_response.xml");
            string myString = myFile.ReadToEnd();
            myFile.Close();
            Parser x = new Parser();
            List<Ticket> t = x.parserequests(myString);
            ticketgrid.ItemsSource = t;
             * */
            App a = (App)Application.Current;
            Parser x = a.p;
            x.url = SDPAPIURL.Text;
            x.tkey = TECHKEY.Text;
            string view = VIEWSTOWATCH.SelectedValue as string;
            x.SendRequestsRequest(x.GetFilterValueByName(view));
            ticketgrid.ItemsSource = x.tickets;
            
            
        }

        private void FilterViewButton_Click(object sender, RoutedEventArgs e)
        {
            App a = (App)Application.Current;
            Parser x = a.p;
            x.url = SDPAPIURL.Text;
            x.tkey = TECHKEY.Text;
            FilterView f = new FilterView(x);
            f.sc = sdpmon.Properties.Settings.Default.ViewsToWatch;
            if (f.sc == null) f.sc = new System.Collections.Specialized.StringCollection();

            f.ShowDialog();
            stc.Clear();
            foreach(string s in sdpmon.Properties.Settings.Default.ViewsToWatch)
            {
                stc.Add(s);
            }
            VIEWSTOWATCH.ItemsSource = stc;

        }

        private void autoenabled_CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            sdpmon.Properties.Settings.Default.autoenabled = false;
            App a = (App)Application.Current;
            a.dt.Stop();
        }

        private void autoenabled_CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            sdpmon.Properties.Settings.Default.autoenabled = true;
            App a = (App)Application.Current;
            a.dt.Start();

        }

        private void browserpickButton_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new Microsoft.Win32.OpenFileDialog() { Filter = "Executable Files (*.exe)|*.exe" };
            var result = ofd.ShowDialog();
            if (result == false) return;
            sdpmon.Properties.Settings.Default.BrowserPath = ofd.FileName;
        }

        private void VIEWSTOWATCH_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (VIEWSTOWATCH.SelectedIndex != -1)
            {
                testenabled = true;
                testButton.IsEnabled = true;
                return;
            }
            testButton.IsEnabled = false;

            testenabled = false;
        }
        
    }
}
