﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace sdpmon
{
    public class Filter
    {
        public string _name;
        public string _value;
        public bool enabled { get; set; }
        public string name { get { return _name; } }
        public string value { get { return _value; } }
        public static List<Filter> LOAD(string filename)
        {
            List<Filter> fl = new List<Filter>();
            try
            {
                string[] data = File.ReadAllLines(filename);
                foreach (string x in data)
                {
                    Filter f = new Filter();
                    string en;
                    string n;
                    string v;
                    string[] rc = x.Split('|');
                    f.enabled = bool.Parse(rc[0]);
                    f._name = rc[1];
                    f._value = rc[2];
                    fl.Add(f);

                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return fl;
        }
        public static void SAVE(List<Filter> fl, string filename)
        {
            List<string> output = new List<string>();
            foreach(Filter f in fl)
            {
                output.Add(f.enabled.ToString() + '|' + f.name + '|' + f.value);
            }
            File.WriteAllLines(filename, output);
        }
        public static List<Filter> LOAD_SC(System.Collections.Specialized.StringCollection sc)
        {
            List<Filter> results = new List<Filter>();
            foreach(string s in sc)
            {
                try{
                string[] wut = s.Split('|');
                Filter f= new Filter();
                f.enabled = bool.Parse(wut[0]);
                f._name = wut[1];
                f._value = wut[2];
                results.Add(f);
                }
                catch { }
            }
            return results;
        }
        public static System.Collections.Specialized.StringCollection SAVE_SC(List<Filter> fl)
        {
            System.Collections.Specialized.StringCollection sc = new System.Collections.Specialized.StringCollection();
            foreach(Filter f in fl)
            {
                string x = f.enabled.ToString() + "|" + f._name + "|" + f._value;
                sc.Add(x);
            }
            return sc;
        }
    }
    public class Ticket
    {
        public int workorderid {get; set;}
        public string priority {get; set;}
        public string subject {get; set;}
        public string createdby {get; set;}
        public string requester {get; set;}
        public string technician {get; set;}
        public long duebytime {get; set;}
        public long createdtime {get; set;}
        public string status {get; set;}
        public bool isoverdue {get; set;}
        public string uri {get; set;}        
    }
}
