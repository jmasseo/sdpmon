﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.Specialized;

namespace sdpmon
{
    /// <summary>
    /// Interaction logic for FilterView.xaml
    /// </summary>
    public partial class FilterView : Window
    {
        
        public List<Filter> _flist;
        public StringCollection sc;
        public Parser p;
        public FilterView()
        {
            InitializeComponent();
            //_flist = Filter.LOAD("FILTERLIST.TXT");
            _flist = new List<Filter>();
            filtdg.ItemsSource = _flist;
        }
        public FilterView(Parser np)
        {
            InitializeComponent();

            p = np;
            _flist = p.filters;
            filtdg.ItemsSource = p.filters;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            foreach(Filter f in p.filters)
            {
                if(f.enabled)
                {
                    try
                    {
                        if (sdpmon.Properties.Settings.Default.ViewsToWatch.Contains(f.name)) continue;
                        sdpmon.Properties.Settings.Default.ViewsToWatch.Add(f.name);
                    }
                    catch (NullReferenceException ex)
                    { }
                }
                else
                {
                    try
                    {
                        if (sdpmon.Properties.Settings.Default.ViewsToWatch.Contains(f.name)) sdpmon.Properties.Settings.Default.ViewsToWatch.Remove(f.name);
                    }
                    catch (NullReferenceException ex)
                    { }
                }
            }
            //sdpmon.Properties.Settings.Default.ViewsToWatch = sc;
            sdpmon.Properties.Settings.Default.FilterList = Filter.SAVE_SC(p.filters);
            sdpmon.Properties.Settings.Default.Save();
            //Filter.SAVE(p.filters, "FILTERLIST.TXT");
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public void SetFlist(List<Filter> f)
        {
            _flist = f;
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            p.SendFiltersRequest();
            try
            {
                foreach (Filter f in p.filters)
                {
                    if (sdpmon.Properties.Settings.Default.ViewsToWatch.Contains(f.name)) f.enabled = true;
                }
            } catch(NullReferenceException ex)
            {

            }
            filtdg.ItemsSource = p.filters;
        }
    }
}
